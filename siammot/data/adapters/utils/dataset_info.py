dataset_maps = dict()
"""
each item in the dataset maps are a list of the following info
(
dataset_folder, 
annotation file name (video dataset) / path of annotation file (image dataset), 
split file name (video dataset) / path of image folder (image dataset) , 
modality
)
"""

dataset_maps['TAO'] = ['TAO',
                       'anno_person.json',
                       'splits_person.json',
                       'video']

dataset_maps['CRP'] = ['caltech_roadside_pedestrians',
                       'anno.json',
                       'splits.json',
                       'video']

dataset_maps['MOT17_DPM'] = ['MOT17',
                             'anno.json',
                             'splits_DPM.json',
                             'video']

dataset_maps['MOT17'] = ['MOT17',
                         'anno.json',
                         'splits.json',
                         'video']

dataset_maps['AOT'] = ['airbone_object_tracking',
                       'anno.json',
                       'splits.json',
                       'video']

dataset_maps['COCO17_train'] = ['mscoco',
                                'annotations/MSCOCO2017_train_person.json',
                                'images/train2017',   # all raw images would be in dataset_root/mscoco/images/train2017
                                'image']

dataset_maps['crowdhuman_train_fbox'] = ['CrowdHuman',
                                         'annotations/annotation_train_fbox.json',
                                         'Images',  # all raw images would be in dataset_root/CrowdHuman/Images
                                         'image']

dataset_maps['crowdhuman_train_vbox'] = ['CrowdHuman',
                                         'annotations/annotation_train_vbox.json',
                                         'Images',
                                         'image']

dataset_maps['crowdhuman_val_fbox'] = ['CrowdHuman',
                                       'annotations/annotation_val_fbox.json',
                                       'Images',
                                       'image']

dataset_maps['crowdhuman_val_vbox'] = ['CrowdHuman',
                                       'annotations/annotation_val_vbox.json',
                                       'Images',
                                       'image']

# Kristyna Janku: Added info for videos from CTC datasets Fluo-N2DH-SIM+, BF-C2DL-MuSC and BF-C2DL-HSC

dataset_maps['Fluo-N2DH-SIM+_train_image_02'] = ['Fluo-N2DH-SIM+',
                                                 'annotation/mot_Fluo-N2DH-SIM+_train_image_02.json',
                                                 'raw_data/train/02',
                                                 'image']

dataset_maps['Fluo-N2DH-SIM+_train_video_02'] = ['Fluo-N2DH-SIM+',
                                                 'Fluo-N2DH-SIM+.json',
                                                 'splits_Fluo-N2DH-SIM+.json',
                                                 'video']

dataset_maps['BF-C2DL-MuSC_train_image_02'] = ['BF-C2DL-MuSC',
                                               'annotation/mot_BF-C2DL-MuSC_train_image_02.json',
                                               'raw_data/train/02',
                                               'image']

dataset_maps['BF-C2DL-MuSC_train_video_02'] = ['BF-C2DL-MuSC',
                                               'BF-C2DL-MuSC.json',
                                               'splits_BF-C2DL-MuSC.json',
                                               'video']

dataset_maps['BF-C2DL-HSC_train_image_01'] = ['BF-C2DL-HSC',
                                              'annotation/mot_BF-C2DL-HSC_train_image_01.json',
                                              'raw_data/train/01',
                                              'image']

dataset_maps['BF-C2DL-HSC_train_video_01'] = ['BF-C2DL-HSC',
                                              'BF-C2DL-HSC.json',
                                              'splits_BF-C2DL-HSC.json',
                                              'video']
